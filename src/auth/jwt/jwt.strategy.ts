import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

// Does the actual validation
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      secretOrKey: 'secretkey',
      ignoreExpiration: false,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    });
  }

  // expose data internally to the rest of the services (req.user)
  async validate(payload: any) {
    // user = await this.usersService.getOne(payload.sub)
    // Grab more info from the database this way.
    return {
      id: payload.sub,
      username: payload.username,
    };
  }
}
